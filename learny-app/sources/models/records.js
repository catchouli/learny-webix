export const data = new webix.DataCollection({
  url: "http://dev.hexon.xyz:13004/cards",
  on: {
    onDataUpdate: (id, data) => {
      webix.ajax().patch("http://dev.hexon.xyz:13004/cards", data)
    }
  }
})
