import {JetView, plugins} from 'webix-jet'
import menu from 'views/menus/sidebar'

import "views/webix/icon";
import "views/webix/menutree";


export default class TopView extends JetView{
  config() {
    return this.ui
  }

  init() {
  }

  get header() {
    return {
      type:"header", template:this.app.config.name
    }
  }

  get content() {
    return (
      { rows:
        [ { type:"clean"
          , css:"app-right-panel"
          , padding:4
          , rows:
            [ { $subview:true } 
            ]
          }
        ]
      }
    )
  }

  get toolbar() {
    return (
      { view: 'toolbar'
      , height: 46
      , cols:
        [ { view: 'label'
          , label: 'Learny'
          }
        ]
      }
    )
  }

  get ui() {
    return (
      { type:"line"
      , rows:
        [ this.toolbar
        , { cols: [ menu, this.content ] }
        ]
      }
    )
  }
}
