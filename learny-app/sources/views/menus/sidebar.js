import {JetView, plugins} from "webix-jet";

const menudata = [
    { id: "overview", value: "Overview", icon: "home", $css: "dashboard", details:"reports and statistics"},
    { id: "add", value: "Add", icon: "check-square-o", $css: "orders", details:"order reports and editing"},
    { id: "review", value: "Review", icon: "cube", $css: "products", details:"all products"},
    { id: "browse", value: "Browse", icon: "pencil-square-o", details: "changing product data"},
    {id: "options", value:"Options", open:1, data:[
      { id: "account", value: "Account", icon: "calendar", details: "calendar example" },
      { id: "files", value: "Log out", icon: "folder-open-o", details: "file manager example" }
    ]}
];

export default class MenuView extends JetView{
  init(){
    webix.$$("app:menu").parse(menudata);
    this.use(plugins.Menu, "app:menu");
  }  
  config(){
    return {
      width: 200,
      view: "tree", id: "app:menu",
      type: "menuTree2", css: "menu",
      activeTitle: true, select: true,
      tooltip: {
        template: function(obj){
          return obj.$count?"":obj.details;
        }
      },
      on:{
      }
    };
  }
}
