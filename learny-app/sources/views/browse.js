import {JetView} from 'webix-jet'
import {data} from 'models/records'

class DataView extends JetView {
  config() {
    return (
      { view:'datatable'
      , columns:
        [ { id: 'english', template: '#english#', header: ['English', { content: 'serverFilter', colspan: 3 }], width: 300, editor: 'text', sort: 'server' }
        , { id: 'japanese', template: '#japanese#', header: 'Japanese', width: 300, editor: 'text', sort: 'server' }
        , { id: 'tags', template: '#tags#', header: 'Tags', width: 300, editor: 'text', sort: 'server' }
        ]
      , editable: true
      , url: "http://dev.hexon.xyz:13004/cards"
      , on:
        { onDataUpdate: (id, data) =>
          {
            webix.ajax().patch("http://dev.hexon.xyz:13004/cards", data)
          }
        }
      , datafetch: 50
      }
    )
  }

  init(view) {
  }
}

export default class Browser extends JetView {
  config() {
    return (
      { rows:
        [ /*{view: 'search', placeholder: 'Search...'}
        ,*/ DataView
        ]
      }
    )
  }
}
