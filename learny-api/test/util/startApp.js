let app

before((done) => {
  console.log('starting app')
  app = require('../../src/api.js').startApp()
  done()
})

after(function(done) {
  app.close()
  console.log('stopping app')
  done()
})
