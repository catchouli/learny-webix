var chai = require('chai');
var chaiAsPromised = require('chai-as-promised')
var alsoChain = require('chai-as-promised-also-chain')
var chaiHttp = require('chai-http')
let expect = chai.expect

chai.use(chaiAsPromised)
chai.use(alsoChain)
chai.use(chaiHttp)

let expectStatus = (res, status) => {
  expect(res).to.have.property('status')
  expect(res.status).to.equal(status)
}

let expectError = (err, status, type) => {
  expectStatus(err, status)
  expect(err).to.have.property('response')
  expect(err.response).to.have.property('body')
  expect(err.response.body).to.have.property('errors')
  expect(err.response.body.errors).to.have.property(type)
}

let expectFailure = () => {
  throw Error('Request should fail')
}

module.exports = chai.expect
module.exports.status = expectStatus
module.exports.error = expectError
module.exports.failure = expectFailure
