'use strict'

const config = require('./config.js')
const mongoose = require('../../src/util/mongoose.js')
const Mockgoose = require('mockgoose').Mockgoose;
const mockgoose = new Mockgoose(mongoose)

mongoose.Promise = require('bluebird')

before((done) => {
  mockgoose.prepareStorage().then(() => {
    exports.conn = mongoose.connection
    exports.conn.on('error', console.error)
    exports.conn.once('open', _ => console.log('connected to db'))
    mongoose.connect(config.mongoUrl, { useMongoClient: true })
    done()
  })
})

// schemas to reset
const Card = require('../../src/schema/card.js')
const User = require('../../src/schema/user.js')
const Deck = require('../../src/schema/deck.js')

beforeEach((done) => {
  // reset schemas
  // note that mockgoose.reset causes the schemas to be destroyed and then
  // we can't easily recreate them due to node's module system and them
  // being created at require() time. instead, just clear them manually
  Promise.all([ Card.remove({}), User.remove({}), Deck.remove({}) ])
    .then(() => {
      done()
    })
})
