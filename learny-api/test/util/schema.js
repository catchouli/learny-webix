'use strict'

const User = require('../../src/schema/user.js')
const Card = require('../../src/schema/card.js')
const Deck = require('../../src/schema/deck.js')

let createTestUser = (email) => {
  let user = new User({email: email, displayName: 'frank'})
  user.setPassword('test')
  return user.save()
}

let createTestDeck = (user, name, parent) => {
  let deck = new Deck({name: name, owner: user._id})
  if (parent)
    deck.parent = parent._id
  return deck.save()
}

let createTestCard = (deck, front, back) => {
  return new Card({fields: { front: front, back: back }, deck: deck._id}).save()
}

module.exports = [User, Deck, Card, createTestUser, createTestDeck, createTestCard]
