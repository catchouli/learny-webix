const Promise = require('bluebird')
const testdb = require('./util/testdb.js')
const expect = require('./util/expect.js')
const config = require('./util/config.js')
const auth = require('../src/controllers/auth.js')
const chai = require('chai');
require('./util/startApp.js')

let baseUrl = 'http://127.0.0.1:' + config.httpPort;

let [User, Deck, Card, createTestUser, createTestDeck, createTestCard] = require('./util/schema.js')

let createTestData = () => {
  return createTestUser('frank@test.com')
    .then((user) => {
      return [user, createTestDeck(user, 'test')]
    })
    .spread((user, deck) => {
      return [user, deck, createTestCard(deck, 'front contents', 'back contents')]
    })
}

describe('/cards', () => {
  describe('GET /cards/{cardId} (get info about a card)', () => {
    it('should not be possible to get info about a nonexistent card', () => {
      return createTestUser('frank@test.com')
        .then((user) => {
          return chai.request(baseUrl).get('/cards/4edd40c86762e0fb12000003')
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'cardId')
        })
    })

    it('should not be possible to get info about another user\'s card', () => {
      return createTestData()
        .spread((user, deck, card) => {
          return [user, deck, card, createTestUser('bob@test.com')]
        })
        .spread((user, deck, card, otherUser) => {
          return chai.request(baseUrl).get('/cards/' + card._id)
            .set('Authorization', 'Bearer ' + auth.genToken(otherUser))
        })
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'Authorization')
        })
    })

    it('should be possible to get info about a card', () => {
      return createTestData()
        .spread((user, deck, card) => {
          return chai.request(baseUrl).get('/cards/' + card._id)
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then((res) => {
          expect.status(res, 200)
          expect(res.body).to.have.property('fields')
          expect(res.body.fields).to.have.property('front')
          expect(res.body.fields).to.have.property('back')
          expect(res.body.fields.front).to.equal('front contents')
          expect(res.body.fields.back).to.equal('back contents')
        })
    })
  })

  describe('PATCH /cards/{cardId} (update a card)', () => {
    it('should not be possible to update a nonexistent card', () => {
      return createTestUser('frank@test.com')
        .then((user) => {
          return chai.request(baseUrl).patch('/cards/4edd40c86762e0fb12000003')
            .query({fields: {x: 5}})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'cardId')
        })
    })

    it('should not be possible to update another user\'s card', () => {
      return createTestData()
        .spread((user, deck, card) => {
          return [user, deck, card, createTestUser('bob@test.com')]
        })
        .spread((user, deck, card, otherUser) => {
          return chai.request(baseUrl).patch('/cards/' + card._id)
            .query({fields: {x: 5}})
            .set('Authorization', 'Bearer ' + auth.genToken(otherUser))
        })
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'Authorization')
        })
    })

    it('should be possible to update a card\'s fields without replacing all of its fields', () => {
      let card
      return createTestData()
        .spread((user, deck, c) => {
          card = c
          return chai.request(baseUrl).patch('/cards/' + card._id)
            .query({fields: {x: 5}})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then((res) => {
          expect.status(res, 200)
          return expect(Card.findOne({_id: card._id})).to.eventually.satisfy((card) => {
            expect(card.fields).to.have.property('front')
            expect(card.fields).to.have.property('back')
            expect(card.fields).to.have.property('x')
            return true
          })
        })
    })

    it('should not be possible to update tags using PATCH', () => {
      let card
      return createTestData()
        .spread((user, deck, c) => {
          card = c
          card.tags = ['x']
          card.markModified('tags')
          return [user, deck, card.save()]
        })
        .spread((user, deck, card) => {
          return chai.request(baseUrl).patch('/cards/' + card._id)
            .query({tags: ['y']})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then((res) => {
          expect.status(res, 200)
          return expect(Card.findOne({_id: card._id})).to.eventually.satisfy((card) => {
            expect(card).to.have.property('tags')
            expect(card.tags).to.have.length(1)
            expect(card.tags[0]).to.equal('x')
            return true
          })
        })
    })
  })

  describe('POST /cards/{cardId} (replace card values)', () => {
    it('should not be possible to update a nonexistent card', () => {
      return createTestUser('frank@test.com')
        .then((user) => {
          return chai.request(baseUrl).post('/cards/4edd40c86762e0fb12000003')
            .query({fields: {x: 5}})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'cardId')
        })
    })

    it('should not be possible to update another user\'s card', () => {
      return createTestData()
        .spread((user, deck, card) => {
          return [user, deck, card, createTestUser('bob@test.com')]
        })
        .spread((user, deck, card, otherUser) => {
          return chai.request(baseUrl).post('/cards/' + card._id)
            .query({fields: {x: 5}})
            .set('Authorization', 'Bearer ' + auth.genToken(otherUser))
        })
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'Authorization')
        })
    })

    it('updating a card\'s fields using POST should replace all its original fields', () => {
      let card
      return createTestData()
        .spread((user, deck, c) => {
          card = c
          return chai.request(baseUrl).post('/cards/' + card._id)
            .query({fields: {x: 5}})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then((res) => {
          expect.status(res, 200)
          return expect(Card.findOne({_id: card._id})).to.eventually.satisfy((card) => {
            expect(card.fields).to.not.have.property('front')
            expect(card.fields).to.not.have.property('back')
            expect(card.fields).to.have.property('x')
            return true
          })
        })
    })

    it('updating a card\'s tags using POST should replace all its original tags', () => {
      let card
      return createTestData()
        .spread((user, deck, c) => {
          card = c
          card.tags = ['x']
          return [user, deck, card.save()]
        })
        .spread((user, deck, card) => {
          return chai.request(baseUrl).post('/cards/' + card._id)
            .query({tags: ['y']})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then((res) => {
          expect.status(res, 200)
          return expect(Card.findOne({_id: card._id})).to.eventually.satisfy((card) => {
            expect(card.tags).to.have.length(1)
            expect(card.tags[0]).to.equal('y')
            return true
          })
        })
    })
  })

  describe('DELETE /card/{cardId} (delete a card)', () => {
    it('should not be possible to delete a nonexistent card', () => {
      return createTestUser('frank@test.com')
        .then((user) => {
          return chai.request(baseUrl).delete('/cards/4edd40c86762e0fb12000003')
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'cardId')
        })
    })

    it('should not be possible to delete another user\'s card', () => {
      return createTestData()
        .spread((user, deck, card) => {
          return [user, deck, card, createTestUser('bob@test.com')]
        })
        .spread((user, deck, card, otherUser) => {
          return chai.request(baseUrl).delete('/cards/' + card._id)
            .set('Authorization', 'Bearer ' + auth.genToken(otherUser))
        })
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'Authorization')
        })
    })

    it('should be possible to delete a card', () => {
      return createTestData()
        .spread((user, deck, card) => {
          return chai.request(baseUrl).delete('/cards/' + card._id)
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then((res) => {
          expect.status(res, 200)
        })
    })
  })

  describe('POST /cards/{cardId}/review', () => {
    it('should not be possible to review a nonexistent card', () => {
      return createTestUser('frank@test.com')
        .then((user) => {
          return chai.request(baseUrl).post('/cards/4edd40c86762e0fb12000003/review')
            .query({score: 3})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'cardId')
        })
    })

    it('should not be possible to review another user\s card', () => {
      return createTestData()
        .spread((user, deck, card) => {
          return [user, deck, card, createTestUser('bob@test.com')]
        })
        .spread((user, deck, card, otherUser) => {
          return chai.request(baseUrl).post('/cards/' + card._id + '/review')
            .query({score: 3})
            .set('Authorization', 'Bearer ' + auth.genToken(otherUser))
        })
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'Authorization')
        })
    })

    it('should be possible to review a card that\'s due', () => {
      return createTestData()
        .spread((user, deck, card) => {
          card.review(2.5)
          card.due = Date.now()
          return [user, deck, card.save()]
        })
        .spread((user, deck, card) => {
          return chai.request(baseUrl).post('/cards/' + card._id + '/review')
            .query({score: 3})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then((res) => {
          expect.status(res, 200)
          expect(res.body).to.have.property('due')
        })
    })

    it('should not be possible to review a card with an invalid score', () => {
      return createTestData()
        .spread((user, deck, card) => {
          card.review(2.5)
          card.due = Date.now()
          return [user, deck, card.save()]
        })
        .spread((user, deck, card) => {
          return chai.request(baseUrl).post('/cards/' + card._id + '/review')
            .query({score: 5})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'score')
        })
    })

    it('should not be possible to review a card without specifying a score', () => {
      return createTestData()
        .spread((user, deck, card) => {
          card.review(2.5)
          card.due = Date.now()
          return [user, deck, card.save()]
        })
        .spread((user, deck, card) => {
          return chai.request(baseUrl).post('/cards/' + card._id + '/review')
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'score')
        })
    })

    it('should not be possible to review a card that\'s not due', () => {
      return createTestData()
        .spread((user, deck, card) => {
          card.review(2.5)
          card.due = Date.now() + 30000
          return [user, deck, card.save()]
        })
        .spread((user, deck, card) => {
          return chai.request(baseUrl).post('/cards/' + card._id + '/review')
            .query({score: 3})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'due')
        })
    })
  })
})
