const jwt = require('jsonwebtoken')
const testdb = require('./util/testdb.js')
const expect = require('./util/expect.js')
const config = require('./util/config.js')
const User = require('../src/schema/user.js')
const chai = require('chai');
require('./util/startApp.js')

let baseUrl = 'http://127.0.0.1:' + config.httpPort;
let usersUrl = baseUrl + '/users'
let authUrl = baseUrl + '/auth'

describe('/auth', () => {
  describe('POST /auth (authenticate a user)', () => {
    let createTestUser = () => {
      let user = new User({email: 'frank@test.com', displayName: 'frank'})
      user.setPassword('lol')
      return user.save()
    }

    it('should not be possible to authenticate the user with the wrong password', () => {
      return createTestUser()
        .then((user) => {
          return chai.request(authUrl)
            .post('/')
            .query({email: 'frank@test.com', password: 'bob'})
            .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'password')
        })
    })

    it('should not be possible to authenticate the user with the wrong email', () => {
      return createTestUser()
        .then((user) => {
          return chai.request(authUrl)
            .post('/')
            .query({email: 'test@test.com', password: 'lol'})
            .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'email')
        })
    })

    it('should be possible to authenticate the user', () => {
      return createTestUser()
        .then((user) => {
          return chai.request(authUrl)
            .post('/')
            .query({email: 'frank@test.com', password: 'lol'})
        })
        .then((res) => {
          expect.status(res, 200)
          expect(res.body).to.have.property('token')

          return chai.request(usersUrl)
            .get('/'+jwt.decode(res.body.token).id)
            .set('Authorization', 'Bearer ' + res.body.token)
        })
        .then((res) => {
          expect.status(res, 200)
          expect(res.body).to.have.property('id')
        })
    })
  })
})
