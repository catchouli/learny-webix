const Promise = require('bluebird')
const testdb = require('./util/testdb.js')
const expect = require('./util/expect.js')
const config = require('./util/config.js')
const auth = require('../src/controllers/auth.js')
const chai = require('chai')
const _ = require('underscore')
const rs = require('random-strings')
require('./util/startApp.js')

let baseUrl = 'http://127.0.0.1:' + config.httpPort;

let [User, Deck, Card, createTestUser, createTestDeck, createTestCard] = require('./util/schema.js')

describe('/decks', () => {
  describe('POST /decks (create a new deck)', () => {
    it('should not be possible to create a new deck when not authenticated', () => {
      return chai.request(baseUrl).post('/decks')
        .query({name: 'bob\'s deck'})
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'Authorization')
          return expect(Deck.find()).to.eventually.be.empty
        })
    })

    it('should be possible to create a new deck', () => {
      return createTestUser('frank@frank.com')
        .then((user) => {
          return chai.request(baseUrl).post('/decks')
            .query({name: 'bob\'s deck'})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
            .then((res) => {
              expect.status(res, 200)
              expect(res.body).to.have.property('id')
              expect(res.body).to.have.property('name')
              return expect(Deck.find()).to.eventually.have.lengthOf(1)
            })
        })
    })
  })

  describe('GET /decks/{deckId} (get info about deck)', () => {
    it('attempting to get a nonexistent deck should cause an error', () => {
      return createTestUser('frank@frank.com')
      .then((user) => {
        return chai.request(baseUrl).get('/decks/4edd40c86762e0fb12000003')
          .set('Authorization', 'Bearer ' + auth.genToken(user))
          .then(expect.failure)
      })
      .catch((err) => {
        expect.error(err, 400, 'deckId')
      })
    })

    it('should not be possible to get info about another user\'s deck', () => {
      return Promise.all([ createTestUser('bob@bob.com'),
                           createTestUser('frank@frank.com').then((user) => createTestDeck(user, "deck")) ])
        .then(([bob, deck]) => {
          return chai.request(baseUrl).get('/decks/' + deck._id).set('Authorization', 'Bearer ' + auth.genToken(bob))
            .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'Authorization')
        })
    })

    it('should be possible to get info about a deck', () => {
      return createTestUser('frank@frank.com').then((user) => [user, createTestDeck(user, "deck")])
        .spread((frank, deck) => {
          return chai.request(baseUrl).get('/decks/' + deck._id).set('Authorization', 'Bearer ' + auth.genToken(frank))
        })
        .then((res) => {
          expect.status(res, 200)
          expect(res.body).to.have.property('id')
          expect(res.body).to.have.property('name')
        })
    })
  })

  describe('DELETE /decks/{deckId} (delete a deck)', () => {
    it('should not be possible to delete a nonexistent deck', () => {
      return createTestUser('frank@frank.com')
        .then((user) => {
          return chai.request(baseUrl).delete('/decks/4edd40c86762e0fb12000003')
            .set('Authorization', 'Bearer ' + auth.genToken(user))
            .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'deckId')
        })
    })

    it('should not be possible to get info about another user\'s deck', () => {
      return Promise.all([ createTestUser('bob@bob.com'),
                           createTestUser('frank@frank.com').then((user) => createTestDeck(user, "deck")) ])
        .then(([bob, deck]) => {
          return chai.request(baseUrl).delete('/decks/' + deck._id).set('Authorization', 'Bearer ' + auth.genToken(bob))
            .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'Authorization')
          return expect(Deck.find()).to.eventually.not.be.empty
        })
    })


    it('should be possible to delete a deck', () => {
      return createTestUser('frank@frank.com').then((user) => [user, createTestDeck(user, "deck")])
        .spread((frank, deck) => {
          return chai.request(baseUrl).delete('/decks/' + deck._id).set('Authorization', 'Bearer ' + auth.genToken(frank))
        })
        .then((res) => {
          expect.status(res, 200)
          return expect(Deck.find()).to.eventually.be.empty
        })
    })
  })

  describe('GET /decks/{deckId}/subdecks (get subdecks of a deck)', () => {
    it('should not be possible to get the subdecks of a deck that doesn\'t exist', () => {
      return createTestUser('frank@frank.com')
        .then((user) => {
          return chai.request(baseUrl).get('/decks/4edd40c86762e0fb12000003/subdecks')
            .set('Authorization', 'Bearer ' + auth.genToken(user))
            .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'deckId')
        })
    })

    it('should not be possible to get the subdecks of another person\'s deck', () => {
      return Promise.all([ createTestUser('bob@bob.com'),
                           createTestUser('frank@frank.com').then((user) => createTestDeck(user, "deck")) ])
        .then(([bob, deck]) => {
          return chai.request(baseUrl).get('/decks/' + deck._id + '/subdecks').set('Authorization', 'Bearer ' + auth.genToken(bob))
            .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'Authorization')
        })
    })

    it('should be possible to get the subdecks of a deck with no subdecks', () => {
      return createTestUser('frank@frank.com').then((user) => [user, createTestDeck(user, "deck")])
        .spread((frank, deck) => {
          return chai.request(baseUrl).get('/decks/' + deck._id + '/subdecks').set('Authorization', 'Bearer ' + auth.genToken(frank))
        })
        .then((res) => {
          expect.status(res, 200)
          expect(res.body.subdecks).to.be.empty
        })
    })

    it('should be possible to get the subdecks of a deck with 1 subdeck', () => {
      return createTestUser('frank@frank.com').then((user) => [user, createTestDeck(user, "deck")])
        .spread((frank, deck) => {
          return [frank, deck, createTestDeck(frank, "child", deck)]
        })
        .spread((frank, deck) => {
          return chai.request(baseUrl).get('/decks/' + deck._id + '/subdecks').set('Authorization', 'Bearer ' + auth.genToken(frank))
        })
        .then((res) => {
          expect.status(res, 200)
          expect(res.body.subdecks).to.have.lengthOf(1)
        })
    })
  })

  describe('GET /decks/{deckId}/cards (get the cards in a deck)', () => {
    it('should not be possible to get the cards in a nonexistent deck', () => {
        return createTestUser('frank@frank.com').then((user) => {
          return chai.request(baseUrl).get('/decks/4edd40c86762e0fb12000003/cards')
          .set('Authorization', 'Bearer ' + auth.genToken(user))
          .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'deckId')
        })
    })

    it('should not be possible to get the cards from a deck that\'s not yours', () => {
      return Promise.all([ createTestUser('bob@bob.com'),
                           createTestUser('frank@frank.com').then((user) => createTestDeck(user, "deck")) ])
        .then(([bob, deck]) => {
          return chai.request(baseUrl).get('/decks/' + deck._id + '/cards').set('Authorization', 'Bearer ' + auth.genToken(bob))
            .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'Authorization')
        })
    })

    it('should be possible to get the cards from a deck', () => {
      return createTestUser('frank@frank.com').then((user) => [user, createTestDeck(user, "deck")])
        .spread((frank, deck) => {
          return [frank, deck, createTestCard(deck, 'hello world', 'bob sucks')]
        })
        .spread((frank, deck) => {
          return chai.request(baseUrl).get('/decks/' + deck._id + '/cards').set('Authorization', 'Bearer ' + auth.genToken(frank))
        })
        .then((res) => {
          expect.status(res, 200)
          expect(res.body.cards).to.have.lengthOf(1)
        })
    })
  })

  describe('POST /decks/{deckId}/cards', () => {
    it('should not be possible to add a new card to a nonexistent deck', () => {
      return createTestUser('bob@bob.com')
        .then((user) => {
          return chai.request(baseUrl).post('/decks/4edd40c86762e0fb12000003/cards')
                  .query({fields: {front: 'x', back: 'y'}, tags: ['x']})
                  .set('Authorization', 'Bearer ' + auth.genToken(user))
                  .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'deckId')
        })
    })

    it('should not be possible to add a card to a deck that\'s not yours', () => {
      return Promise.all([ createTestUser('bob@bob.com'),
                           createTestUser('frank@frank.com').then((user) => createTestDeck(user, "deck")) ])
        .then(([bob, deck]) => {
          return chai.request(baseUrl).post('/decks/' + deck._id + '/cards')
            .set('Authorization', 'Bearer ' + auth.genToken(bob))
            .query({fields: {front: 'x', back: 'y'}, tags: ['x']})
            .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'Authorization')
        })
    })

    it('should be possible to add a new card to a deck', () => {
       return createTestUser('frank@frank.com').then((user) => [user, createTestDeck(user, "deck")])
        .spread((frank, deck) => {
          return chai.request(baseUrl).post('/decks/' + deck._id + '/cards')
            .set('Authorization', 'Bearer ' + auth.genToken(frank))
            .query({fields: {front: 'x', back: 'y'}, tags: ['x']})
        })
        .then((res) => {
          expect.status(res, 200)
          expect(res.body).to.have.property('id')
        })
    })
  })

  describe('GET /decks/{deckId}/cards/due (get the due cards from a deck)', () => {
    it('should not be possible to get the due cards in a nonexistent deck', () => {
      return createTestUser('bob@bob.com')
        .then((user) => {
          return chai.request(baseUrl).get('/decks/4edd40c86762e0fb12000003/cards/due')
            .set('Authorization', 'Bearer ' + auth.genToken(user))
            .then(expect.failure)
            .catch((err) => {
              expect.error(err, 400, 'deckId')
            })
        })
    })

    it('should not be possible to get the due cards from a deck that\'s not yours', () => {
      return Promise.all([ createTestUser('bob@bob.com'),
                           createTestUser('frank@frank.com').then((user) => createTestDeck(user, "deck")) ])
        .then(([bob, deck]) => {
          return chai.request(baseUrl).get('/decks/' + deck._id + '/cards/due').set('Authorization', 'Bearer ' + auth.genToken(bob))
            .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'Authorization')
        })
    })

    it('should be possible to get the due cards from a deck', () => {
      return createTestUser('frank@frank.com').then((user) => [user, createTestDeck(user, "deck")])
        .spread((frank, deck) => {
          let cardDue = createTestCard(deck, 'hello world', 'bob sucks')
            .then((card) => {
              card.review(2.5)
              card.due = Date.now()
              return card.save();
            })

          let cardNotDue = createTestCard(deck, 'sorry bob', 'bob doesn\'t suck')
            .then((card) => {
              card.review(2.5)
              return card.save();
            })

          return [frank, deck, cardDue, cardNotDue]
        })
        .spread((frank, deck, cardDue, cardNotDue) => {
          return [ chai.request(baseUrl).get('/decks/' + deck._id + '/cards/due')
                      .set('Authorization', 'Bearer ' + auth.genToken(frank))
                 , cardDue, cardNotDue]
        })
        .spread((res, cardDue, cardNotDue) => {
          expect.status(res, 200)
          expect(res.body.cards).to.contain(cardDue._id.toString())
          expect(res.body.cards).to.not.contain(cardNotDue._id.toString())
        })
    })
  })
})
