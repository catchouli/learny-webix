const Promise = require('bluebird')
const testdb = require('./util/testdb.js')
const expect = require('./util/expect.js')
const config = require('./util/config.js')
const User = require('../src/schema/user.js')
const auth = require('../src/controllers/auth.js')
const chai = require('chai');
require('./util/startApp.js')

let baseUrl = 'http://127.0.0.1:' + config.httpPort;
let usersUrl = baseUrl + '/users'

const createUser = (email, displayName, password) => {
  let user = new User({email: email, displayName: displayName})
  user.setPassword(password)
  return user.save()
}

describe('/users', () => {
  describe('POST /users (create a new user)', () => {
    it('should not be possible to create a user without a valid email', () => {
      return chai.request(baseUrl).post('/users')
        .query({email: 'tehuaoeu', displayName: 'hello', password: 'test1234'})
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'email')
          return expect(User.find({email: 'tehuaoeu'})).to.eventually.be.empty
        })
    })

    it('should not be possible to create a user without a valid password', () => {
      return chai.request(usersUrl).post('/')
        .query({email: 'test2@test.com', displayName: 'hello', password: ''})
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'password')
          return expect(User.find({email: 'test2@test.com'})).to.eventually.be.empty
        })
    })

    it('should not be possible to create a user without a valid display name', () => {
      return chai.request(usersUrl).post('/')
        .query({email: 'test3@test.com', displayName: '', password: 'test1234'})
        .then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'displayName')
          return expect(User.find({email: 'test3@test.com'})).to.eventually.be.empty
        })
    })

    it('should be possible to create a user, but not reuse its email', () => {
      const buildQuery = () => chai.request(usersUrl)
                                   .post('/')
                                   .query({email: 'test1@test.com', displayName: 'hello', password: 'test1234'})
      return buildQuery()
        .then((res) => {
          expect.status(res, 200)
          expect(res.body).to.have.property('id')
          return expect(User.find({email: 'test1@test.com'})).to.eventually.have.lengthOf(1)
        })
        .then(() => {
          // retry to make sure it fails the second time because the email is the same
          return buildQuery().then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'email')
          return expect(User.find({email: 'test1@test.com'})).to.eventually.have.lengthOf(1)
        })
    })
  })

  describe('GET /users/{userId} (request user details)', () => {
    it('a nonexistent user should result in an error', () => {
      return chai.request(usersUrl).get('/348294')
        .query({id: 0}).then(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'id')
        })
    })

    it('should be posible to query a user\'s display name', () => {
      return createUser('frank@frank.com', 'frank', 'test1234')
        .then((user) => {
          return [user._id, chai.request(usersUrl).get('/' + user._id)]
        })
        .spread((id, res) => {
          expect.status(res, 200)
          expect(res.body).to.have.property('id')
          expect(res.body).to.have.property('displayName')
          expect(res.body.displayName).to.equal('frank')

          return expect(User.findOne({_id: id})).to.eventually.satisfy((user) => {
            return user.displayName == res.body.displayName
          })
        })
    })

    it('should not be possible to query a user\'s email without being logged in', () => {
      return createUser('frank@frank.com', 'frank', 'test1234')
        .then((user) => {
          return chai.request(usersUrl).get('/' + user._id)
        })
        .then((res) => {
          expect.status(res, 200)
          expect(res.body).to.not.have.property('email')
        })
    })

    it('should be possible to query a user\'s email if logged in', () => {
      return createUser('frank@frank.com', 'frank', 'test1234')
        .then((user) => {
          return chai.request(usersUrl).get('/' + user._id).set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then((res) => {
          expect.status(res, 200)
          expect(res.body.email).to.equal('frank@frank.com')
        })
    })
  })

  describe('POST /users/{userId} (update user details)', () => {
    it('should not be possible to update a user\'s display name without being authenticated', () => {
      return createUser('frank@frank.com', 'frank', 'test1234')
        .then((user) => {
          return chai.request(usersUrl).post('/'+user._id).query({displayName: 'mr blobby'})
                    .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'Authorization')

          return expect(User.findOne({email: 'frank@frank.com'})).to.eventually.satisfy((user) => {
            return user.displayName == 'frank'
          })
        })
    })

    it('should not be possible to update a user\'s email without being authenticated', () => {
      return createUser('frank@frank.com', 'frank', 'test1234')
        .then((user) => {
          return chai.request(usersUrl).post('/'+user._id).query({email: 'blob@blob.com'})
                    .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'Authorization')

          return expect(User.findOne({displayName: 'frank'})).to.eventually.satisfy((user) => {
            return user.email == 'frank@frank.com'
          })
        })
    })

    it('should not be possible to update a password without being authenticated', () => {
      return createUser('frank@frank.com', 'frank', 'test1234')
        .then((user) => {
          return chai.request(usersUrl).post('/'+user._id).query({password: 'blob@blob.com', currentPassword: 'test1234'})
                    .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'Authorization')

           return expect(User.findOne({displayName: 'frank'})).to.eventually.satisfy((user) => {
            return user.comparePassword('test1234')
          })
        })
    })

    it('should be possible to update a user\'s display name if authenticated', () => {
      return createUser('frank@frank.com', 'frank', 'test1234')
        .then((user) => {
          return chai.request(usersUrl).post('/'+user._id)
            .query({displayName: 'har har'})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then((res) => {
          expect.status(res, 200)

          return expect(User.findOne({email: 'frank@frank.com'})).to.eventually.satisfy((user) => {
            return user.displayName == 'har har'
          })
        })
    })

    it('should not be possible to update a user\'s email without their current password', () => {
      return createUser('frank@frank.com', 'frank', 'test1234')
        .then((user) => {
          return chai.request(usersUrl).post('/'+user._id)
            .query({email: 'bob@bob.com'})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
            .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'currentPassword')

          return expect(User.findOne({email: 'frank@frank.com'})).to.eventually.not.be.empty
        })
    })

    it('should not be possible to update a user\'s password without their current password', () => {
      return createUser('frank@frank.com', 'frank', 'test1234')
        .then((user) => {
          return chai.request(usersUrl).post('/'+user._id)
            .query({password: 'boobies'})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
            .then(expect.failure)
        })
        .catch((err) => {
          expect(err, 400, 'currentPassword')

          return expect(User.findOne({email: 'frank@frank.com'})).to.eventually.satisfy((user) => {
            return user.comparePassword('test1234')
          })
        })
    })

    it('should be possible to update a user\'s email with their current password', () => {
      return createUser('frank@frank.com', 'frank', 'test1234')
        .then((user) => {
          return chai.request(usersUrl).post('/'+user._id)
            .query({email: 'bob@bob.com', currentPassword: 'test1234'})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then((res) => {
          expect.status(res, 200)

          return expect(User.findOne({displayName: 'frank'})).to.eventually.satisfy((user) => {
            return user.email == 'bob@bob.com'
          })
        })
    })

    it('should be possible to update a user\'s password with their current password', () => {
      return createUser('frank@frank.com', 'frank', 'test1234')
        .then((user) => {
          expect(user.comparePassword('test1234')).to.equal(true)

          return chai.request(usersUrl).post('/'+user._id)
            .query({password: 'boobies', currentPassword: 'test1234'})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
        })
        .then((res) => {
          expect.status(res, 200)

          return expect(User.findOne({displayName: 'frank'})).to.eventually.satisfy((user) => {
            return user.comparePassword('boobies')
          })
        })
    })

    it('should not be able to update a user with an invalid email address', () => {
      return createUser('frank@frank.com', 'frank', 'test1234')
        .then((user) => {
          return chai.request(usersUrl).post('/' + user._id)
            .query({email: 'godjesus', currentPassword: 'test1234'})
            .set('Authorization', 'Bearer ' + auth.genToken(user))
            .then(expect.failure)
        })
        .catch((err) => {
          expect.error(err, 400, 'email')
        })
    })

    it('should not be possible to update another user\'s display name', () => {
      let updateDisplayName = (id, newName, authToken) => {
        return chai.request(usersUrl).post('/' + id)
          .query({displayName: newName})
          .set('Authorization', 'Bearer ' + authToken)
      }

      return Promise.all([createUser('frank@frank.com', 'frank', 'test1234'), createUser('bob@bob.com', 'bob', 'bobrulez')])
        .then(([frank, bob]) => {
          return [frank, bob, updateDisplayName(frank._id, 'frank rulez111', auth.genToken(frank))]
        })
        .spread((frank, bob, res) => {
          expect.status(res, 200)
          
          return [frank, bob, updateDisplayName(frank._id, 'frank suxxx', auth.genToken(bob))]
        })
        .spread(expect.failure)
        .catch((err) => {
          expect.error(err, 400, 'Authorization')
        })
    })
  })
})
