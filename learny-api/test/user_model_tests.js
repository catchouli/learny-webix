const testdb = require('./util/testdb.js')
const expect = require('./util/expect.js')
const config = require('./util/config.js')
const User = require('../src/schema/user.js')

describe('User model', function() {
  describe('creation', function() {
    let createUser = () => {
      let user = new User({displayName: 'test', email: 'test@test.com'})
      user.setPassword('test123123')
      return user.save()
    }

    it('should be possible to make and save a user', function() {
      return expect(createUser()).to.not.be.rejected
    })

    it('should not be possible to make and save a user with duplicate details', function() {
      return expect(createUser()).to.not.be.rejected
        .then(() => {
          return expect(createUser()).to.be.rejected
        })
    })

    it('should not be possible to save a user without a password', function() {
      let user = new User({displayName: 'test', email: 'aeuaoeu@eou.com'})
      return expect(user.save()).to.be.rejected
    })

    it('should not be possible to look up a nonexistent user', function() {
      return User.find({email: 'nonexistent@test.com'})
                 .then((res) => {
                   expect(res).to.be.empty
                 })
    })

    it('should be possible to look up our created user', function() {
      return User.find({email: 'nonexistent@test.com'})
                 .then((res) => {
                   expect(res).to.be.empty
                 })
    })

    it('should not be possible to authenticate a user with no password', function() {
      expect(new User().comparePassword('')).to.equal(false)
    })

    it('should not be possible to authenticate our test user with the wrong password', function() {
      return createUser()
               .then((res) => {
                 expect(res.comparePassword('test1234')).to.be.equal(false)
               })
    })

    it('should be possible to authenticate our test user with the right password', function() {
      return createUser()
               .then((res) => {
                 expect(res.comparePassword('test123123')).to.be.equal(true)
               })
    })

    it('shouldn\'t be possible to authenticate our test user after changing password', function() {
      return createUser()
               .then((res) => {
                 res.setPassword('aoe')
                 expect(res.comparePassword('test123123')).to.be.equal(false)
               })
    })
  })
})
