'use strict'

const config = require('../config.js')
const mongoose = require('../util/mongoose.js')

mongoose.Promise = require('bluebird')

exports.conn = mongoose.connection
exports.conn.on('error', console.error)
exports.conn.once('open', _ => console.log('connected to db'))
mongoose.connect(config.mongoUrl, { useMongoClient: true })
