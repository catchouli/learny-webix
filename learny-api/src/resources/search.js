'use strict'

// Connect to elasticsearch
exports.client = new (require('elasticsearch')).Client({
  host: 'localhost:9200',
  log: 'warning'
})
