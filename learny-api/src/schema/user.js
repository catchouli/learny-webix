'use strict'

let mongoose = require('../util/mongoose.js')
let search = require('../resources/search.js')
let bcrypt = require('bcrypt')

let userSchema = mongoose.Schema(
  { displayName:
    { type: String
    , trim: true
    , required: true
    }
  , email:
    { type: String
    , unique: true
    , lowercase: true
    , trim: true
    , required: true
    }
  , hash_password:
    { type: String
    , required: true
    }
  , created:
    { type: Date
    , default: Date.now
    }
  }, { usePushEach: true })

userSchema.methods.setPassword = function(password) {
  this.hash_password = bcrypt.hashSync(password, bcrypt.genSaltSync())
}

userSchema.methods.comparePassword = function(password) {
  if (!this.hash_password)
    return false;
  else
    return bcrypt.compareSync(password, this.hash_password)
}

userSchema.methods.checkPermission = function(requester) {
  return requester._id.equals(this._id)
}

let User = mongoose.model('User', userSchema)

module.exports = User
