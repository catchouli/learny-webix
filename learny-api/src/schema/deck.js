'use strict'

const Card = require('./card.js')
const mongoose = require('mongoose')

let deckSchema = mongoose.Schema({
  name: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  parent: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Deck'
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
}, { usePushEach: true })

deckSchema.methods.findCards = function() {
  // todo: this family of functions only finds cards that are direct ancestors
  return Card.find({deck: this._id})
}

deckSchema.methods.findNewCards = function() {
  return Card.find({deck: this._id, state: 'new'})
}

deckSchema.methods.findCurrentlyDue = function() {
  let dayStart, dayEnd
  [dayStart, dayEnd] = Card.getDay()

  // needs to be syncronised with the code in card.isDue
  // unfortunately duplicated cuz this needs to be done in a db query
  return Card
    .find({ deck: this._id
          , $or:
            [ { state: 'learning', due: { $lt: Date.now() } }
            , { state: 'learnt', due: { $lt: dayEnd } }
            ]
          })
}

deckSchema.methods.findDueToday = function() {
  let dayStart, dayEnd
  [dayStart, dayEnd] = Card.getDay()

  return Card
    .find({ deck: this._id
          , due: { $lt: dayEnd }
          })
}

deckSchema.methods.checkPermission = function(user) {
  return user._id.equals(this.owner)
}

let Deck = mongoose.model('Deck', deckSchema)

module.exports = Deck
