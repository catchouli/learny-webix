'use strict'

let search = require('../resources/search.js')
let mongoose = require('mongoose')

let cardSchema = mongoose.Schema({
  fields: {
    type: mongoose.Schema.Types.Mixed,
    default: {},
    required: true
  },
  tags: {
    type: [String],
    default: []
  },
  deck: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Deck',
    required: true
  },
  state: {
    type: String,
    enum: ['new', 'learning', 'learnt'],
    default: 'new',
    required: true
  },
  created: {
    type: Date,
    default: Date.now,
    required: true
  },
  // The card's ease, goes up if reviews are scored higher or down if lower
  ease: {
    type: Number,
    default: 2.5,
    required: true
  },
  // The card's interval, a card that has not finished learning yet has no interval
  interval: {
    type: Number
  },
  // When the card is next due, new cards have no due field
  due: {
    type: Date
  },
  // The review step is the number of times this card's been reviewed without
  // lapsing it, and is used to know when we're out of learning
  step: {
    type: Number,
    default: 0,
    required: true
  }
}, { usePushEach: true })

let calculateEase = (currentEase, score) => {
  // from SM-2 page https://www.supermemo.com/english/ol/sm2.htm
  return Math.max(currentEase - 0.8 + 0.28 * score - 0.02 * score * score, 1.3)
}

cardSchema.methods.review = function (score) {
  // todo: the currently constant learning steps can be exposed
  // as a configuration
  const learningSteps = [ 60, 10 * 60 ]

  if (this.step < learningSteps.length) {
    // Do learning steps initially
    // When we're learning don't modify the ease
    this.interval = learningSteps[this.step]
    this.state = 'learning'
  }
  else if (this.step == learningSteps.length) {
    // After the card gets out of learning bump the interval to one day
    // (assuming the learning steps din't already do that)
    this.interval = Math.max(this.interval, 24 * 60 * 60)
    this.state = 'learnt'
  }
  else {
    this.ease = calculateEase(this.ease, score)
    this.interval = this.interval * this.ease
    this.state = 'learnt'
  }

  this.step = this.step + 1
  this.due = new Date(Date.now() + this.interval * 1000)
}

cardSchema.methods.isDue = function() {
  // needs to be synchronized with the logic in deck.findCurrentlyDue
  // unfortunately duplicated since that's done in the db for performance
  let [dayStart, dayEnd] = Card.getDay()
  return (this.state == 'learning' && this.due < Date.now()) ||
         (this.state == 'learnt' && this.due < dayEnd)
}

cardSchema.methods.checkPermission = function(user) {
  const Deck = require('./deck.js')
  return Deck.findOne({_id: this.deck}).then((deck) => { return user._id.equals(deck.owner) })
}

// create model
let Card = mongoose.model('Card', cardSchema)

// baseQuery is a mongoose collection, e.g. Card or Card.find(something)
Card.filter = function(baseQuery, query, start, len, sortField, sortDir) {
  let sort = {}
  if (sortField && sortDir) {
    sort['fields.' + sortField] = sortDir  
  }

  // Avoid doing a search lookup if unnecessary
  if (!query) {
    return Promise.all([ baseQuery().count()
                       , baseQuery().count()
                       , baseQuery().find().sort(sort).skip(start).limit(len)
                       ])
  }
  else {
    return search.client.search({ index: 'learny'
                                , type: 'cards'
                                , body:
                                  { query:
                                    { simple_query_string:
                                      { query: query }
                                    }
                                  }
                                })
            .then(res => res.hits.hits.map(hit => hit._id))
            .then(ids => {
              // Use a function so we aren't manipulating the same query object twice
              let query = (() => baseQuery().find({'_id': { $in: ids }}))
              return Promise.all([baseQuery().count(), query().count(), query().sort(sort).skip(start).limit(len)])
            })
  }
}

Card.getDay = function() {
  // todo: we can expose this as a parameter, currently it's 4AM utc
  const resetTime = 4

  // Show learning cards when they're actually due (since they tend to have intervals of under a day and showing them immediately wouldn't be that good)
  // Show all other cards due today immediately
  const now = new Date()
  const dayStart = new Date(now.getFullYear(), now.getMonth(), now.getDate(), resetTime, 0, 0)
  const dayEnd = new Date(dayStart.getTime() + 24 * 60 * 60 * 1000)

  return [dayStart, dayEnd]
}

module.exports = Card
