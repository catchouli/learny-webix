'use strict'

const express = require('express')
const router = express.Router()
const util = require('./util.js')
const User = require('../schema/user.js')
const auth = require('./auth.js')

const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

router.post('/', (req, res) => {
  res.setHeader('Content-Type', 'application/json')

  req.checkQuery({
    email: {
      notEmpty: true,
      isEmail: true
    },
    password: {
      notEmpty: true
    },
    displayName: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  let user = new User({email: req.query.email, displayName: req.query.displayName})
  user.setPassword(req.query.password)
  user.save()
    .then((r) => {
      res.status(200)
      res.send({id:r._id})
    })
    .catch((r) => {
      let result = {errors: {}}
      if (r && r.code == 11000) {
        result.errors['email'] = ['Already in use.']
      }
      else {
        result.errors[''] = 'Unknown error'
      }
      res.status(400)
      res.send(result)
    })
})

router.get('/:userId', (req, res) => {
  res.setHeader('Content-Type', 'application/json')

  req.query.id = req.params.userId

  req.checkQuery({
    id: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  // currently there's no public information about a user
  // todo: some sort of permissions for access to resources
  User.findOne({_id: req.query.id})
    .then((user) => {
      let response = ({id: user._id, displayName: user.displayName})

      // allow the user to query their own private data if authenticated
      if (req.user && user.checkPermission(req.user)) {
        response.email = req.user.email
      }

      res.status(200)
      res.send(response)
    })
    .catch(() => {
      res.status(400)
      res.send({errors: {"id": 'No such user'}})
    })
})

router.post('/:userId', auth.required, (req, res) => {
  res.setHeader('Content-Type', 'application/json')

  req.query.id = req.params.userId

  req.checkQuery({
    email: {
      optional: true,
      isEmail: true
    },
    password: {
      optional: true,
      notEmpty: true
    },
    currentPassword: {
      optional: true,
      notEmpty: true
    },
    displayName: {
      optional: true,
      notEmpty: true
    },
    id: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  // find user by id
  User.findOne({_id: req.query.id})
    .then((user) => {
      // authenticate user
      if (user && !user.checkPermission(req.user)) {
        auth.failure(res)
        return
      }

      if (req.query.email || req.query.password) {
        if (!req.query.currentPassword) {
          res.status(400)
          res.send({errors: {'currentPassword': 'A current password is needed to update a user\'s email address or password'}})
          return
        }
        else if (!user.comparePassword(req.query.currentPassword)) {
          res.status(400)
          res.send({errors: {'currentPassword': 'Incorrect password'}})
          return
        }
      }

      if (req.query.email)
        user.email = req.query.email
      if (req.query.password)
        user.setPassword(req.query.password)
      if (req.query.displayName)
        user.displayName = req.query.displayName

      user.save()
        .then(() => {
          res.status(200)
          res.send({})
          return
        })
        .catch(() => {
          res.status(400)
          res.send({errors: {'': 'An unknown error occurred'}})
          return
        })
    })
    .catch(() => {
      res.status(400)
      res.send({errors: {'id': 'User not found'}})
      return
    })
})

module.exports = router
