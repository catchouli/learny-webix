'use strict'

const config = require('../config.js')
const User = require('../schema/user.js')
const util = require('./util.js')
const jwt = require('jsonwebtoken')
const express = require('express')

exports.middleware = (req, res, next) => {
  req.user = null
  let authHeader = req.header('Authorization')
  if (authHeader) {
    if (authHeader.slice(0,7) == "Bearer ") {
      let authToken = authHeader.slice(7)
      jwt.verify(authToken, config.authSecret, (err, decoded) => {
        if (err) {
          res.status(400)
          res.send({errors: {"Authorization": err.message}})
        }
        else {
          if (decoded.id) {
            User.findOne({_id: decoded.id})
              .then((user) => {
                req.user = user
                next()
              })
              .catch(() => {
                res.status(400)
                res.send({errors: {"Authorization": 'Failed to get authenticated user information'}})
              })
          }
          else {
            res.status(400)
            res.send({errors: {"Authorization": 'Token did not contain required data'}})
          }
        }
      })
    }
    else {
      res.status(400)
      res.send({errors: {"Authorization": 'Invalid auth method'}})
    }
  }
  else {
    next()
  }
}

exports.genToken = (user) => {
  return jwt.sign({ id: user._id}, config.authSecret)
}

exports.router = express.Router()

exports.router.post('/', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  
  req.checkQuery({
    email: {
      notEmpty: true,
      isEmail: true
    },
    password: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  User.findOne({email: req.query.email}).then()
      .then((user) => {
        if (user.comparePassword(req.query.password)) {
          res.status(200)
          res.send({token: exports.genToken(user)})
        }
        else {
          res.status(400)
          res.send({errors: {'password': 'Invalid password'}})
        }
      })
      .catch(() => {
        res.status(400)
        res.send({errors: {'email': 'No user found with specified email'}})
      })
})

exports.required = (req, res, next) => {
  if (!req.user) {
    exports.failure(res)
    return
  }

  next()
}

exports.failure = (res) => {
  res.status(400)
  res.send({errors: {'Authorization': 'Authentication is required to access this resource'}})
}
