'use strict'

const express = require('express')
const router = express.Router()
const util = require('./util.js')
const auth = require('./auth.js')
const Card = require('../schema/card.js')

const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

function requestCard(cardId, user, res, cb) {
  Card.findOne({_id: cardId})
    .then((card) => {
      return [card, card ? card.checkPermission(user) : false]
    })
    .spread((card, hasPermission) => {
      if (card == null) {
        res.status(400)
        res.send({errors: {cardId: 'No such card'}})
        return
      }
      else if (!hasPermission)
      {
        auth.failure(res)
        return
      }
      else {
        return cb(card)
      }
    })
    .catch((err) => {
      console.error(err)
      res.status(400)
      res.send({errors: {"": 'An unknown error occurred'}})
    })
}

function validateFields(req, res) {
  if (!req.query)
    return true
  if (!req.query.fields)
    return true

  let fields = req.query.fields

  if (typeof fields !== 'object') {
    res.status(400)
    res.send({errors: {"fields": 'must be object'}})
    return false
  }

  let keys = Object.keys(fields)

  if (keys.length > 20) {
    res.status(400)
    res.send({errors: {"fields": 'cannot have more than 20 entries'}})
    return false
  }

  for (let i = 0; i < keys.length; ++i) {
    if (typeof(fields[keys[i]]) !== 'string') {
      res.status(400)
      res.send({errors: {"fields": 'values must be strings'}})
      return false
    }
  }
  
  return true
}

function validateTags(req, res) {
  if (!req.query)
    return true
  if (!req.query.tags)
    return true
  if (typeof req.query.tags === 'string')
    req.query.tags = [req.query.tags]

  let tags = req.query.tags

  if (!Array.isArray(tags) && tags.constructor !== Array) {
    res.status(400)
    res.send({errors: {"tags": 'must be array'}})
    return false
  }

  for (let i = 0; i < tags.length; ++i) {
    if (typeof tags[i] !== 'string') {
      res.status(400)
      res.send({errors: {"tags": 'values must be strings'}})
      return false
    }
  }
  
  return true
}

router.get('/:cardId', auth.required, (req, res) => {
  res.setHeader('Content-Type', 'application/json')

  req.query.cardId = req.params.cardId

  req.checkQuery({
    cardId: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  requestCard(req.query.cardId, req.user, res, (card) => {
    res.status(200)    
    res.send(card.toObject())
  })
})

router.post('/:cardId', auth.required, (req, res) => {
  res.setHeader('Content-Type', 'application/json')

  req.query.cardId = req.params.cardId

  req.checkQuery({
    cardId: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  if (!validateFields(req, res) || !validateTags(req, res))
    return

  requestCard(req.query.cardId, req.user, res, (card) => {
    if (req.query.fields) {
      card.fields = req.query.fields
      card.markModified('fields')
    }
    if (req.query.tags) {
      card.tags = req.query.tags
    }

    return card.save()
      .then((card) => {
        res.status(200)    
        res.send({})
      })
  })

})

router.patch('/:cardId', auth.required, (req, res) => {
  res.setHeader('Content-Type', 'application/json')

  req.query.cardId = req.params.cardId

  req.checkQuery({
    cardId: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  if (!validateFields(req, res))
    return

  requestCard(req.query.cardId, req.user, res, (card) => {
    if (req.query.fields) {
      let keys = Object.keys(req.query.fields)
      for (let i = 0; i < keys.length; ++i) {
        card.fields[keys[i]] = req.query.fields[keys[i]]
        card.markModified('fields')
      }
    }

    return card.save()
      .then((card) => {
        res.status(200)    
        res.send({})
      })
  })
})

router.delete('/:cardId', auth.required, (req, res) => {
  res.setHeader('Content-Type', 'application/json')

  req.query.cardId = req.params.cardId

  req.checkQuery({
    cardId: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  requestCard(req.query.cardId, req.user, res, (card) => {
    return card.remove()
      .then(() => {
        res.status(200)
        res.send({})
      })
  })
})

router.post('/:cardId/review', auth.required, (req, res) => {
  const possibleScores = [1,2,3,4]

  res.setHeader('Content-Type', 'application/json')

  req.query.cardId = req.params.cardId

  req.checkQuery({
    cardId: {
      notEmpty: true
    },
    score: {
      notEmpty: true,
      isInt: true
    }
  })

  if (util.checkErrors(req, res))
    return

  if (!(req.query.score in possibleScores)) {
    res.status(400)
    res.send({errors: {score: 'Score must be one of ' + possibleScores}})
    return
  }

  requestCard(req.query.cardId, req.user, res, (card) => {
    if (!card.isDue()) {
      res.status(400)
      res.send({errors: {due: 'card is not yet due'}})
      return
    }

    card.review(req.query.score)

    return card.save()
      .then((card) => {
        res.status(200)
        res.send({due: card.due})
      })
  })
})

/////////////////////////////////// old //////////////////////////////
router.get('/', auth.required, (req, res) => {
    res.setHeader('Content-Type', 'application/json')

    req.checkQuery({
      start: {
        optional: true,
        isInt: true
      },
      len: {
        optional: true,
        isInt: true
      },
      sortDir: {
        optional: true,
        isIn: ['asc', 'desc']
      }
    })

    if (util.checkErrors(req, res))
      return

    let start = req.query.start ? parseInt(req.query.start) : 0
    let len = req.query.count ? parseInt(req.query.count) : 30

    let filterKeys = req.query.filter ? Object.keys(req.query.filter) : null
    let search = filterKeys && filterKeys.length > 0 ? req.query.filter[filterKeys[0]] : null

    let sortKeys = req.query.sort ? Object.keys(req.query.sort) : null
    let sortCol = sortKeys && sortKeys.length > 0 ? sortKeys[0] : null
    let sortDir = sortCol ? req.query.sort[sortCol] : null

    // todo: this is inefficient since it does the search twice
    let ownerCards = () => Card.find({owner:req.user._id})
    Card.filter(ownerCards, search, start, len, sortCol, sortDir)
      .then(([total, filtered, cards]) => {
        res.send(
          { data: cards.map(card => {
            let cardData = {}
            let fieldNames = Object.keys(card.fields)
            for (let i = 0; i < fieldNames.length; ++i)
              cardData[fieldNames[i]] = card.fields[fieldNames[i]]
            cardData._id = card.id
            return cardData
          })
          , recordsTotal: total
          , recordsFiltered: filtered
          , pos: start
          , total_count: filtered
          })
      })
      .catch((err) => {
        console.error(err)
        res.send({ error: "An error occurred" })
      })
})

router.patch('/', auth.required, function(req, res) {
  res.setHeader('Content-Type', 'application/json')

  req.checkQuery({
    id: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  // todo: verify data a bit better probably
  if (req.body._id) {
    Card.findOne({_id: req.body._id})
      .then((card) => {
        let fields = Object.keys(req.body)
        for (let i = 0; i < fields.length; ++i) {
          if (fields[i][0] != '_') {
            card.fields[fields[i]] = req.body[fields[i]]
          }
        }
        card.markModified('fields')
        card.save()
      })
      .then(() => {
        res.setHeader('Content-Type', 'application/json')
        res.send({})
      })
      .catch((err) => {
        console.error(err)
        res.send({ error: "An error occurred" })
      })
  }
})

router.post('/', auth.required, function(req, res) {
  res.setHeader('Content-Type', 'application/json')

  let card = new Card({fields:{}, owner: req.user._id})

  let fieldNames = Object.keys(req.body)
  for (let i = 0; i < fieldNames.length; ++i) {
    card.fields[fieldNames[i]] = req.body[fieldNames[i]]
  }

  card.save()
    .then((card) => {
      res.status(200)
      res.send({id: card._id})
    })
    .catch((err) => {
      res.status(400)
      res.send({errors: {'': 'An unknown error occurred when trying to create card'}})
    })
})
/////////////////////////////////// old //////////////////////////////

module.exports = router
