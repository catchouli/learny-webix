'use strict'

exports.checkErrors = (req, res) => {
  let errors = req.validationErrors()
  if (errors) {
    let result = {errors: {}}
    errors = errors.map((val) => {
      result.errors[val.param] = val.msg
    })

    res.status(400)
    res.send(result)
    return true
  }
  else {
    return false
  }
}

