'use strict'

const express = require('express')
const router = express.Router()
const util = require('./util.js')
const auth = require('./auth.js')
const Card = require('../schema/card.js')
const Deck = require('../schema/deck.js')

const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

function requestDeck(deckId, user, res, cb) {
  Deck.findOne({_id: deckId})
    .then((deck) => {
      if (deck == null) {
        res.status(400)
        res.send({errors: {deckId: 'No such deck'}})
        return
      }
      else if (!deck.checkPermission(user))
      {
        auth.failure(res)
        return
      }
      else {
        return cb(deck)
      }
    })
    .catch((err) => {
      console.error(err)
      res.status(400)
      res.send({errors: {"": 'An unknown error occurred'}})
    })
}

router.post('/', auth.required, (req, res) => {
  res.setHeader('Content-Type', 'application/json')

  req.checkQuery({
    name: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  new Deck({name: req.query.name, owner: req.user}).save()
    .then((deck) => {
      res.send({id: deck._id, name: deck.name})
    })
    .catch(() => {
      res.status(400)
      res.send({errors: {"": 'An unknown error occurred'}})
    })
})

router.get('/:deckId', auth.required, function(req, res) {
  res.setHeader('Content-Type', 'application/json')

  req.query.deckId = req.params.deckId

  req.checkQuery({
    deckId: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  requestDeck(req.query.deckId, req.user, res, (deck) => {
    res.status(200)
    res.send({id: deck._id, name: deck.name})
  })
})

router.delete('/:deckId', auth.required, function(req, res) {
  res.setHeader('Content-Type', 'application/json')

  req.query.deckId = req.params.deckId

  req.checkQuery({
    deckId: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  requestDeck(req.query.deckId, req.user, res, (deck) => {
    res.status(200)
    return deck.remove()
      .then(() => {
        res.status(200)
        res.send({id: deck._id, name: deck.name})
      })
  })
})

router.get('/:deckId/subdecks', auth.required, function(req, res) {
  res.setHeader('Content-Type', 'application/json')

  req.query.deckId = req.params.deckId

  req.checkQuery({
    deckId: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  requestDeck(req.query.deckId, req.user, res, (deck) => {
    return Deck.find({parent: deck._id})
      .then((decks) => {
        res.status(200);
        res.send({subdecks: decks.map(deck => deck._id)})
      })
  })
})

router.get('/:deckId/cards', auth.required, function(req, res) {
  res.setHeader('Content-Type', 'application/json')

  req.query.deckId = req.params.deckId

  req.checkQuery({
    deckId: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  requestDeck(req.query.deckId, req.user, res, (deck) => {
    return deck.findCards()
      .then((cards) => {
        res.status(200);
        res.send({cards: cards.map(card => card._id)})
      })
  })
})

router.get('/:deckId/cards/due', auth.required, function(req, res) {
  res.setHeader('Content-Type', 'application/json')

  req.query.deckId = req.params.deckId

  req.checkQuery({
    deckId: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  requestDeck(req.query.deckId, req.user, res, (deck) => {
    return deck.findCurrentlyDue()
      .then((cards) => {
        res.status(200);
        res.send({cards: cards.map(card => card._id)})
      })
  })
})

router.post('/:deckId/cards', auth.required, function(req, res) {
  res.setHeader('Content-Type', 'application/json')

  req.query.deckId = req.params.deckId

  req.checkQuery({
    deckId: {
      notEmpty: true
    }
  })

  if (util.checkErrors(req, res))
    return

  requestDeck(req.query.deckId, req.user, res, (deck) => {
    let card = new Card({deck: deck._id})
    if (req.query.tags)
      card.tags = req.query.tags
    if (req.query.fields)
      card.fields = req.query.fields

    return card.save()
      .then((card) => {
        res.status(200)
        res.send({id: card._id})
      })
  })
})

module.exports = router
