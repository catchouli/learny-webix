if (!Array.prototype.contains)
  Array.prototype.contains = function(val) { return this.indexOf(val) != -1 }
