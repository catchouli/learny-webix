let csvjson = require('csvjson')
let sc = require('./schema.js')

let cards = csvjson.toObject(fs.readFileSync('data/tmp.txt', {encoding: 'utf8'}), {delimiter: '\t'})

cards.map((fields) => new sc.Card({fields: fields}).save())
