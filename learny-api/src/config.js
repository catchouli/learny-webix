'use strict';

const crypto = require('crypto')

// Default parameters
exports.salt = '$2a$04$g5sY3nOfWguDwp/8bazp1.';
exports.httpPort = 13004;
exports.environment = (typeof process.env.LEARNY_ENV == 'undefined' ? 'development' : process.env.LEARNY_ENV);
exports.mongoUrl = 'mongodb://localhost:27017/learny';
exports.authSecret = crypto.randomBytes(256)

// If we have a config override, load it
const fs = require('fs');
if (fs.existsSync('configurations/' + exports.environment + '.js')) {
  let overrides = require('../configurations/' + exports.environment + '.js');
  let keys = Object.keys(overrides);
  for (let i = 0; i < keys.length; ++i) {
    exports[keys[i]] = overrides[keys[i]];
  }
}
else {
  console.log("Loading default environment (environments/" + exports.environment + ".js not found)");
}
