'use strict'


const express = require('express')
const cors = require('cors')
const config = require('./config.js')
const auth = require('./controllers/auth.js')
const bodyParser = require('body-parser')
const validator = require('express-validator')

exports.startApp = function() {
  let app = express()

  // Express plugins
  app.use(bodyParser.urlencoded({extended: true}));
  app.use(bodyParser.json())
  app.use(cors())
  app.use(validator())
  app.use(auth.middleware)

  // Controllers
  app.use('/cards', require('./controllers/card_controller.js'))
  app.use('/users', require('./controllers/user_controller.js'))
  app.use('/decks', require('./controllers/deck_controller.js'))
  app.use('/auth', auth.router)

  // Start listening
  return app.listen(config.httpPort, () => {
    console.log('HTTP listening on ' + config.httpPort)
  })
}
